var IZIMIKY = "izimiky_13";
var DEFAULT_SOUND_VOLUME = 100;

var HAMMER_ROTATION_DEFAULT = new Rotation3D(0, 0, 1, 0);
var HAMMER_SLOPE_DEFAULT = new Rotation3D(1, 0, 0, -60);

/**
 * Угол поворота
 * @type {Rotation3D}
 */
var HAMMER_SCALE_DEFAULT = new Scale3D(5, 5, 5);

var TUNING_FORK_ROTATION_DEFAULT = new Rotation3D(0, 0, 1, 0);
var TUNING_FORK_SLOPE_DEFAULT = new Rotation3D(1, 0, 0, -60);
var TUNING_FORK_SLOPE_FOR_OPERATING_TABLE = new Rotation3D(1, 0, 0, 0.01);

var operatingTable		= scene.getObjectByName("rgr-operating-table");
var hammersTable		= scene.getObjectByName("rgr-hammers-table");
var tuningForksTable	= scene.getObjectByName("rgr-tuning-fork-table");
var hammers = [
    scene.getObjectByName("rgr-hammer-0"),
    scene.getObjectByName("rgr-hammer-1"),
    scene.getObjectByName("rgr-hammer-2"),
    scene.getObjectByName("rgr-hammer-3")
];
var tuningForks = [
    scene.getObjectByName("rgr-tuning-fork-0"),
    scene.getObjectByName("rgr-tuning-fork-1"),
    scene.getObjectByName("rgr-tuning-fork-2"),
    scene.getObjectByName("rgr-tuning-fork-3"),
    scene.getObjectByName("rgr-tuning-fork-4"),
    scene.getObjectByName("rgr-tuning-fork-5"),
    scene.getObjectByName("rgr-tuning-fork-6")
];

// Инициализация кнопок управления игрой
var playBtn = scene.getObjectByName("playBtn");
var stopBtn = scene.getObjectByName("stopBtn");
var noteBox = scene.getObjectByName("noteBox");
var helpBtn = scene.getObjectByName("helpBtn");

// Инициализация ботов
var teacher		= scene.getBotByName("teacher");
var boy			= scene.getBotByName("boy");
var girl		= scene.getBotByName("girl");
var copyNoteBox	= scene.getBotByName("copyNoteBox");

// Состояние игры
var gs = [];
    gs.gameIsPlay = false;
    gs.boyNeedsChooseHammer = false;
    gs.boyNeedsChooseTuningFork = false;
    gs.noteBoxIsPlay = false;
    gs.randomNoteIndex = -1;
    gs.randomVolumeIndex = -1;
    gs.selectHammerIndex = null;
    gs.selectTuningForkIndex = null;
    gs.hammerOldPosition = null;
    gs.tuningForkOldPosition = null;
var successPlayTuningSoundFunc = null;

// Звуки камертона
scene.note_c = getServerResourceByUser("note_c0db.mp3", IZIMIKY);
scene.note_d = getServerResourceByUser("note_d0db.mp3", IZIMIKY);
scene.note_e = getServerResourceByUser("note_e0db.mp3", IZIMIKY);
scene.note_f = getServerResourceByUser("note_f0db.mp3", IZIMIKY);
scene.note_g = getServerResourceByUser("note_g0db.mp3", IZIMIKY);
scene.note_a = getServerResourceByUser("note_a0db.mp3", IZIMIKY);
scene.note_h = getServerResourceByUser("note_h0db.mp3", IZIMIKY);

scene.ok_itIsC = getServerResourceByUser("pravil'no_jeto_zvuk_c.mp3", IZIMIKY);
scene.ok_itIsD = getServerResourceByUser("pravil'no_jeto_zvuk_d.mp3", IZIMIKY);
scene.ok_itIsE = getServerResourceByUser("pravil'no_jeto_zvuk_e.mp3", IZIMIKY);
scene.ok_itIsF = getServerResourceByUser("pravil'no_jeto_zvuk_f.mp3", IZIMIKY);
scene.ok_itIsG = getServerResourceByUser("pravil'no_jeto_zvuk_g.mp3", IZIMIKY);
scene.ok_itIsA = getServerResourceByUser("pravil'no_jeto_zvuk_a.mp3", IZIMIKY);
scene.ok_itIsH = getServerResourceByUser("pravil'no_jeto_zvuk_h.mp3", IZIMIKY);

// Разговоры героев игры
scene.sound_kamertonJeto                    = getServerResourceByUser("1.kamerton_jeto.mp3", IZIMIKY);
scene.sound_aJetotZvukZavisitOtTogo         = getServerResourceByUser("2.a_jetot_zvuk_zavisit_ot_togo.mp3", IZIMIKY);
scene.sound_aEsliUdaritPoKamertonuSilnee    = getServerResourceByUser("3.a_esli_udarit'_po_kamertonu_sil'nee.mp3", IZIMIKY);
scene.sound_sejchasvyUslyshiteZvuk          = getServerResourceByUser("4.sejchas_vy_uslyshite_zvuk.mp3", IZIMIKY);
scene.sound_chtobyVybratKamertonIMolotok    = getServerResourceByUser("5.chtoby_vybrat'_kamerton_i_molotok.mp3", IZIMIKY);
scene.sound_chtobyPovtoritZvuk              = getServerResourceByUser("6.chtoby_povtorit'_zvuk.mp3", IZIMIKY);
scene.sound_chtobyVybratKamerton            = getServerResourceByUser("8.chtoby_vybrat'_kamerton.mp3", IZIMIKY);
scene.sound_netNadoXvukVyshe                = getServerResourceByUser("10-а.net_nado_zvuk_vyshe.mp3", IZIMIKY);
scene.sound_netNadoZvukNizhe                = getServerResourceByUser("10-b.net_nado_zvuk_nizhe.mp3", IZIMIKY);
scene.sound_molodecTeperPodberemGromkost    = getServerResourceByUser("11.molodec_teper'_podberem_gromkost'.mp3", IZIMIKY);
scene.sound_chtobyVybratMolotok             = getServerResourceByUser("12.chtoby_vybrat'_molotok.mp3", IZIMIKY);
scene.sound_netNadoGromche                  = getServerResourceByUser("14-a.net_nado_gromche.mp3", IZIMIKY);
scene.sound_netNadoTishe                    = getServerResourceByUser("14-b.net_nado_tishe.mp3", IZIMIKY);
scene.sound_molodecPravilno                 = getServerResourceByUser("15.molodec_pravil'no.mp3", IZIMIKY);
scene.sound_aTeperVyberiKamertonDljaJetogo  = getServerResourceByUser("16.a_teper'_vyberi_kamerton_dlja_jetogo.mp3", IZIMIKY);

// задаем операционному столу его величину и угол поворота
function InitOperatingTable() {
    operatingTable.rotation = new Rotation3D(0, 0, 1, 90);
    operatingTable.scale = new Scale3D(5.5, 5.5, 5.5);
    traceStr("inited operation table.");
} // Сделано

// задаем столу, на кторых лежат молоточки, величину и угол поворота
function InitHammerTable() {
    hammersTable.rotation = new Rotation3D(0, 0, 1, 90);
    hammersTable.scale = new Scale3D(3.66, 3.66, 3.66);
    traceStr("inited hammer table.");
} // Сделано

// раскладываем молоточки на стол
function InitHammers() {
    for (var i = 0; i < hammers.length; i++) {
        hammers[i].position = hammersTable.position;
    }

    hammers[0].position.x += 60;
    hammers[1].position.x += 20;
    hammers[2].position.x -= 20;
    hammers[3].position.x -= 60;

    hammers[0].position.z += 45;
    hammers[1].position.z += 42;
    hammers[2].position.z += 40;
    hammers[3].position.z += 40;

    for (var i = 0; i < hammers.length; i++) {
        hammers[i].rotation = HAMMER_ROTATION_DEFAULT;
        hammers[i].rotation = HAMMER_SLOPE_DEFAULT;
        hammers[i].scale = HAMMER_SCALE_DEFAULT;
    }
    traceStr("inited hammers.");
} // Сделано

// задаем столу, на кторых лежат камертоны, величину и угол поворота
function InitTuningForkTable() {
    tuningForksTable.rotation = new Rotation3D(0, 0, 1, 90);
    tuningForksTable.scale = new Scale3D(3.66, 3.66, 3.66);
    traceStr("inited tuning-fork table.");
} // Сделано

// раскладываем камертоны на стол
function InitTuningForks() {
    for (var i = 0; i < tuningForks.length; i++) {
        tuningForks[i].position = tuningForksTable.position;
    }

    tuningForks[0].position.x += 120;
    tuningForks[1].position.x += 80;
    tuningForks[2].position.x += 40;
    tuningForks[3].position.x += 0;
    tuningForks[4].position.x -= 40;
    tuningForks[5].position.x -= 80;
    tuningForks[6].position.x -= 120;

    for (var i = 0; i < tuningForks.length; i++) {
        tuningForks[i].position.z += 40;
        tuningForks[i].rotation = TUNING_FORK_ROTATION_DEFAULT;
        tuningForks[i].rotation = TUNING_FORK_SLOPE_DEFAULT;
    }

    tuningForks[0].scale = new Scale3D(2.20, 2.20, 2.20);
    tuningForks[1].scale = new Scale3D(2.05, 2.05, 2.05);
    tuningForks[2].scale = new Scale3D(1.90, 1.90, 1.90);
    tuningForks[3].scale = new Scale3D(1.75, 1.75, 1.75);
    tuningForks[4].scale = new Scale3D(1.60, 1.60, 1.60);
    tuningForks[5].scale = new Scale3D(1.45, 1.45, 1.45);
    tuningForks[6].scale = new Scale3D(1.30, 1.30, 1.30);
    traceStr("inited tuning-forks.");
} // Сделано

// инициализируем объекты сцен, возможна только вне игры
function InitScene() {
    traceStr("Start function InitScene.");

    if (!gs.gameIsPlay) {
        InitOperatingTable();
        InitHammerTable();
        InitHammers();
        InitTuningForkTable();
        InitTuningForks();
        showMessageBox("Игра", "Сцена настроена.");
    } else {
        showMessageBox("Игра", "Нельзя во время игры настроить сцену. Настраивайте её перед началом игры.");
    }

    traceStr("End function InitScene.");
} // Сделано

function GetRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
} // Сделано

function GetNoteByIndex(index) {
    var note = null;
    switch (index) {
        case 0: note = scene.note_c; break;
        case 1: note = scene.note_d; break;
        case 2: note = scene.note_e; break;
        case 3: note = scene.note_f; break;
        case 4: note = scene.note_g; break;
        case 5: note = scene.note_a; break;
        case 6: note = scene.note_h; break;
        default: traceStr("undefined note index"); break;
    }
    return note;
} // Сделано

function GetNoteVolumeByIndex(index) {
    var volume = null;
    switch (index) {
        case 0: volume = 100; break;
        case 1: volume = 75; break;
        case 2: volume = 50; break;
        case 3: volume = 25; break;
        default: traceStr("undefined note volume index"); break;
    }
    return volume
} // Сделано

// Воспроизведение загаданного звука с запретом на повторное воспроизведение,
// пока не проигран предыдущий запрос
scene.PlayTuningSound = function() {
    traceStr("Start function PlayTuningSound.");
    if (gs.noteBoxIsPlay) {
        traceStr("can not play notebox, because it play this moment.");
        showMessageBox("Коробка со звуком", "Звук был воспроизведен ранее, дождитесь его завершения.");
        return;
    }

    if (gs.randomNoteIndex == -1 || gs.randomVolumeIndex == -1) {
        traceStr("can not play notebox, because not play game or not init note.");
        showMessageBox("Коробка со звуком", "Коробка может воспроизводить звук только во время игры после загадывания ноты.");
        return;
    }

    gs.noteBoxIsPlay = true;
    traceStr("play note \"" + gs.randomNoteIndex + "\" with power \"" + gs.randomVolumeIndex + "\".");
    copyNoteBox.playSound(GetNoteByIndex(gs.randomNoteIndex), GetNoteVolumeByIndex(gs.randomVolumeIndex));
    copyNoteBox.onSoundPlayed = function() {
        traceStr("played note \"" + gs.randomNoteIndex + "\" with power \"" + gs.randomVolumeIndex + "\".");
        gs.noteBoxIsPlay = false;
        if (successPlayTuningSoundFunc !== null) {
            successPlayTuningSoundFunc();
            successPlayTuningSoundFunc = null;
        }
    }
    traceStr("End function PlayTuningSound.");
} // Сделано

// Удаляем события выбора молоточков
function ClearSelectEventsForHammers() {
    traceStr("Start function ClearSelectEventsForHammers.");

    for (var i = 0; i < hammers.length; i++) {
        hammers[i].onPress = null;
    }

    traceStr("End function ClearSelectEventsForHammers.");
} // Сделано

// Вешаем события выбора на молоточки
function InitSelectEventsForHammers() {
    traceStr("Start function InitSelectEventsForHammers.");

    var SelectedEvent = function(index) {
        gs.selectHammerIndex = index;
        gs.boyNeedsChooseHammer = false;
        ClearSelectEventsForHammers();
        traceStr("select hammer index = \"" + index + "\".");
        Item13Begin();
    };

    hammers[0].onPress = function() { SelectedEvent(0); };
    hammers[1].onPress = function() { SelectedEvent(1); };
    hammers[2].onPress = function() { SelectedEvent(2); };
    hammers[3].onPress = function() { SelectedEvent(3); };

    traceStr("Start function InitSelectEventsForHammers.");
} // Сделано

// Удаляем события выбора камертонов
function ClearSelectEventsForTuningForks() {
    traceStr("Start function ClearSelectEventsForTuningForks.");

    for (var i = 0; i < tuningForks.length; i++) {
        tuningForks[i].onPress = null;
    }

    traceStr("End function ClearSelectEventsForTuningForks.");
} // Сделано

// Вешаем события выбора на камертоны
function InitSelectEventsForTuningForks() {
    traceStr("Start function InitSelectEventsForTuningFork.");

    var SelectedEvent = function(index) {
        gs.selectTuningForkIndex = index;
        gs.boyNeedsChooseTuningFork = false;
        ClearSelectEventsForTuningForks();
        traceStr("select tuning-fork index = \"" + index + "\".");
        Item9Begin();
    };

    tuningForks[0].onPress = function() { SelectedEvent(0); };
    tuningForks[1].onPress = function() { SelectedEvent(1); };
    tuningForks[2].onPress = function() { SelectedEvent(2); };
    tuningForks[3].onPress = function() { SelectedEvent(3); };
    tuningForks[4].onPress = function() { SelectedEvent(4); };
    tuningForks[5].onPress = function() { SelectedEvent(5); };
    tuningForks[6].onPress = function() { SelectedEvent(6); };

    traceStr("End function InitSelectEventsForTuningFork.");
} // Сделано

function GetHammerByIndex(index) {
    for (var i = 0; i < hammers.length; i++) {
        if (i == index) {
            return hammers[i];
        }
    }
    return null;
} // Сделано

function GetTuningForkByIndex(index) {
    for (var i = 0; i < tuningForks.length; i++) {
        if (i == index) {
            return tuningForks[i];
        }
    }
    return null;
} // Сделано

function Help() {
    if (!gs.gameIsPlay) {
        showMessageBox("Игра", "Чтобы начать игру, нажмите на кнопку Play.");
    } else if (gs.boyNeedsChooseTuningFork) {
        scene.Item8();
    } else if (gs.boyNeedsChooseHammer) {
        Item12();
    } else {
        showMessageBox("Игра", "Следите за игрой.");
    }
}

// Начало игры, учительница начинает рассказывать, что такое камертон
function StartGame() {
    traceStr("Start function StartGame.");

    if (!gs.gameIsPlay) {
        gs.gameIsPlay = true;
        gs.boyNeedsChooseHammer = false;
        teacher.playSound(scene.sound_kamertonJeto, DEFAULT_SOUND_VOLUME);
        teacher.onSoundPlayed = Item2;
    } else {
        showMessageBox("Игра", "Повторно игру можно запустить только после завершения текущей.");
    }

    traceStr("End function StartGame.");
}

function StopGame() {
    traceStr("Start function StopGame.");

    gs.gameIsPlay = false;
    gs.randomNoteIndex = -1;
    gs.randomVolumeIndex = -1;
    gs.noteBoxIsPlay = false;

    teacher.stopSounds();
    boy.stopSounds();
    girl.stopSounds();
    copyNoteBox.stopSounds();

    traceStr("End function StopGame.");
}

// Девочка расскзывает, от чего зависит тональность звука камертона
function Item2() {
    traceStr("Start function Item2.");

    girl.playSound(scene.sound_aJetotZvukZavisitOtTogo, DEFAULT_SOUND_VOLUME);
    girl.onSoundPlayed = Item3;

    traceStr("End function Item2.");
} // Сделано

// Мальчик рассказывает, от чего зависит громкость звука камертона
function Item3() {
    traceStr("Start function Item3.");

    boy.playSound(scene.sound_aEsliUdaritPoKamertonuSilnee, DEFAULT_SOUND_VOLUME);
    boy.onSoundPlayed = Item4;

    traceStr("End function Item3.");
} // Сделано

// Учительница говорит, что сейчас мы услышим звук и попытаемся подобрать такой же
function Item4() {
    traceStr("Start function Item4.");

    teacher.playSound(scene.sound_sejchasvyUslyshiteZvuk, DEFAULT_SOUND_VOLUME);
    teacher.onSoundPlayed = Item5;

    traceStr("End function Item4.");
} // Сделано

// Мальчик рассказывает, как нам выбрать камертон и молоток
function Item5() {
    traceStr("Start function Item5.");

    boy.playSound(scene.sound_chtobyVybratKamertonIMolotok, DEFAULT_SOUND_VOLUME);
    boy.onSoundPlayed = Item6;

    traceStr("End function Item5.");
} // Сделано

// Учительница рассказывает, как можно будет повторно воспроизвести загаданный звук
function Item6() {
    traceStr("Start function Item6.");

    teacher.playSound(scene.sound_chtobyPovtoritZvuk, DEFAULT_SOUND_VOLUME);
    teacher.onSoundPlayed = Item7;

    traceStr("End function Item6.");
} // Сделано

// Загадываем ноту и его громкость воспроизведения, а потом воспроизводим его
function Item7() {
    traceStr("Start function Item7.");

    gs.randomNoteIndex = GetRandomInt(0, 6);
    gs.randomVolumeIndex = GetRandomInt(0, 3);
    traceStr("random note = \"" + gs.randomNoteIndex + "\" and volume power = \"" + gs.randomVolumeIndex + "\".");

    successPlayTuningSoundFunc = scene.Item8;
    playByTimer("PlayTuningSound", 2000);

    traceStr("End function Item7.");
} // Сделано

// Мальчик объясняет, как выбрать камертон
scene.Item8 = function() {
    traceStr("Start function Item8.");

    if (!gs.gameIsPlay) {
        traceStr("exit function Item8, because play game stoped.");
        return;
    }

    boy.playSound(scene.sound_chtobyVybratKamerton, DEFAULT_SOUND_VOLUME);
    boy.onSoundPlayed = function() {
        gs.boyNeedsChooseTuningFork = true;
        InitSelectEventsForTuningForks();
    };

    traceStr("End function Item8.");
} // Сделано

// Позиционируем камертон и самый большой молоток
function Item9Begin() {
    traceStr("Start function Item9Begin.");

    if (!gs.gameIsPlay) {
        traceStr("exit function Item9Begin, because play game stoped.");
        return;
    }

    var tuningFork = GetTuningForkByIndex(gs.selectTuningForkIndex);
    var hammer = GetHammerByIndex(0);

    gs.hammerOldPosition = hammer.position;
    gs.tuningForkOldPosition = tuningFork.position;

    var tuningForkPosition = new Position3D(operatingTable.position.x, operatingTable.position.y, operatingTable.position.z);
    tuningForkPosition.z += 100 + (6 - gs.selectTuningForkIndex) * 5;
    var hammerPosition = new Position3D(operatingTable.position.x, operatingTable.position.y, operatingTable.position.z);

    hammerPosition.y += 20;
    hammerPosition.z += 100 + (6 - gs.selectTuningForkIndex) * 7;
    hammerPosition.x += 40;

    tuningFork.rotation = TUNING_FORK_SLOPE_FOR_OPERATING_TABLE

    tuningFork.moveByTime(null, tuningForkPosition, 1000);
    hammer.moveByTime(null, hammerPosition, 1000);

    playByTimer("Item9End", 3000);

    traceStr("End function Item9Begin.");
}

// воспроизводим звук выбранного камертона на полной громкости
scene.Item9End = function() {
    traceStr("Start function Item9End.");

    if (!gs.gameIsPlay) {
        traceStr("exit function Item9Begin, because play game stoped.");
        return;
    }

    copyNoteBox.playSound(GetNoteByIndex(gs.selectTuningForkIndex), DEFAULT_SOUND_VOLUME);
    copyNoteBox.onSoundPlayed = Item10;

    traceStr("End function Item9End.");
};

// Сделано

// проверяем правильность подбора тональности звука
function Item10() {
    traceStr("Start function Item10.");

    if (!gs.gameIsPlay) {
        traceStr("exit function Item10, because play game stoped.");
        return;
    }

    if (gs.selectTuningForkIndex == gs.randomNoteIndex) {
        InitHammers();
        Item11();
    } else {
        var boySound = (gs.selectTuningForkIndex < gs.randomNoteIndex) ? scene.sound_netNadoXvukVyshe : scene.sound_netNadoZvukNizhe;
        boy.playSound(boySound, DEFAULT_SOUND_VOLUME);
        boy.onSoundPlayed = function() {
            gs.boyNeedsChooseTuningFork = true;
            InitSelectEventsForTuningForks();
            InitHammers();
            InitTuningForks();
        };
    }

    traceStr("End function Item10.");
} // Сделано

// учительница хвалит игрока и предлагает подобрать громковть звука
function Item11() {
    traceStr("Start function Item11.");

    if (!gs.gameIsPlay) {
        traceStr("exit function Item11, because play game stoped.");
        return;
    }

    teacher.playSound(scene.sound_molodecTeperPodberemGromkost, DEFAULT_SOUND_VOLUME);
    teacher.onSoundPlayed = Item12;

    traceStr("End function Item11.");
} // Сделано

// мальчик объясняет, как выбрать молоток
function Item12() {
    traceStr("Start function Item12.");

    if (!gs.gameIsPlay) {
        traceStr("exit function Item12, because play game stoped.");
        return;
    }

    boy.playSound(scene.sound_chtobyVybratMolotok, DEFAULT_SOUND_VOLUME);
    boy.onSoundPlayed = function() {
        gs.boyNeedsChooseHammer = true;
        InitSelectEventsForHammers();
    };

    traceStr("End function Item12.");
} // Сделано

function Item13Begin() {
    traceStr("Start function Item13Begin.");

    if (!gs.gameIsPlay) {
        traceStr("exit function Item13Begin, because play game stoped.");
        return;
    }

    var hammer = GetHammerByIndex(gs.selectHammerIndex);

    var hammerPosition = new Position3D(operatingTable.position.x, operatingTable.position.y, operatingTable.position.z);

    hammerPosition.y += 20;
    hammerPosition.z += 100 + (6 - gs.selectTuningForkIndex) * 7;
    hammerPosition.x += 40;

    hammer.moveByTime(null, hammerPosition, 1000);

    playByTimer("Item13End", 3000);

    traceStr("End function Item13Begin.");
}

scene.Item13End = function () {
    traceStr("Start function Item13End.");

    if (!gs.gameIsPlay) {
        traceStr("exit function Item13End, because play game stoped.");
        return;
    }

    copyNoteBox.playSound(GetNoteByIndex(gs.selectTuningForkIndex), GetNoteVolumeByIndex(gs.selectHammerIndex));
    copyNoteBox.onSoundPlayed = Item14;

    traceStr("End function Item13End.");
};

function Item14() {
    traceStr("Start function Item14.");

    if (!gs.gameIsPlay) {
        traceStr("exit function Item14, because play game stoped.");
        return;
    }

    if (gs.selectHammerIndex == gs.randomVolumeIndex) {
        InitHammers();
        Item15();
    } else {
        var boySound = (gs.selectHammerIndex < gs.randomVolumeIndex) ? scene.sound_netNadoTishe : scene.sound_netNadoGromche;
        boy.playSound(boySound, DEFAULT_SOUND_VOLUME);
        boy.onSoundPlayed = function() {
            gs.boyNeedsChooseHammer = true;
            InitSelectEventsForHammers();
            InitHammers();
        };
    }

    traceStr("End function Item14.");
}

function Item15() {
    traceStr("Start function Item15.");

    if (!gs.gameIsPlay) {
        traceStr("exit function Item15, because play game stoped.");
        return;
    }

    var teacherSound = null;
    switch (gs.randomNoteIndex) {
        case 0: teacherSound = scene.ok_itIsC; break;
        case 1: teacherSound = scene.ok_itIsD; break;
        case 2: teacherSound = scene.ok_itIsE; break;
        case 3: teacherSound = scene.ok_itIsF; break;
        case 4: teacherSound = scene.ok_itIsG; break;
        case 5: teacherSound = scene.ok_itIsA; break;
        case 6: teacherSound = scene.ok_itIsH; break;
        default: traceStr("undefined note randomNoteIndex"); break;
    }
    teacher.playSound(teacherSound, DEFAULT_SOUND_VOLUME);
    teacher.onSoundPlayed = StopGame;

    traceStr("End function Item15.");
}

operatingTable.onPress = InitScene;
playBtn.onPress = StartGame;
stopBtn.onPress = StopGame;
noteBox.onPress = scene.PlayTuningSound;
helpBtn.onPress = Help;